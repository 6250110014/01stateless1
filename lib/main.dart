import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.star),
          title: Text('Code App'),
          actions: [IconButton(onPressed: () {}, icon: Icon(Icons.language))],
        ),
        body: Center(
          child: Text(
            'Sakared Nakprasit',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
          ),
        ),
      ),
    );
  }
}
